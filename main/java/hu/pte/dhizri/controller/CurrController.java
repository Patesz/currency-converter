package hu.pte.dhizri.controller;

import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.StorageService;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.DropdownButton;
import com.gluonhq.charm.glisten.mvc.View;

import hu.pte.dhizri.Config;
import hu.pte.dhizri.model.CurrencyPairs;
import hu.pte.dhizri.model.Serialize;
import hu.pte.dhizri.my.MyDropdownButton;
import hu.pte.dhizri.GluonApplication;
import hu.pte.dhizri.exc.UnsupportedCurrencyException;
import hu.pte.dhizri.model.LatestCurrDAO;
import hu.pte.dhizri.util.fx.*;
import hu.pte.dhizri.util.java.FileUtil;
import hu.pte.dhizri.util.java.MathUtil;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

//import com.gluonhq.charm.down.plugins.DeviceService

class PrevInput implements Serializable {
    private static final long serialVersionUID = 2L;

    public double value;
    public int from;
    public int to;

    public PrevInput(){}

    public PrevInput(double value, int from, int to){
        this.value = value;
        this.from = from;
        this.to = to;
    }
}

public class CurrController implements Initializable {

    @FXML private View home;
    @FXML private AnchorPane containerPane;

    @FXML private VBox vbox;

    @FXML private Label fromToLbl;
    @FXML private Label resultLbl;
    @FXML private Label currencyTag;

    @FXML private MyDropdownButton currencyFrom, currencyTo;

    @FXML private Spinner<Double> amountSpinner;

    @FXML private ImageView arrowImgVw;

    @FXML private Label infoLbl;

    @FXML private Button closeApp;

    //@FXML private DropdownButton colorPicker;

    private LatestCurrDAO latestDAO = null;
    private Serialize<Map<String, Double>> currencySer = null;
    private Serialize<PrevInput> prevSer = null;

    private Map<String,Double> cd = null;

    private String latestDate = DateUtil.getToday("YYYY-MM-dd");

// region Signature
    private static final Signature signature = new Signature();

    public static class Signature {
        private Signature() {}
    }
// endregion

// region Instance
    private static CurrController instance;
    public CurrController(){
        instance = this;
    }

    public static CurrController getInstance(GluonApplication.Signature sg){
        if(sg == null){
            throw new NullPointerException("Method signature is null!");
        }
        return instance;
    }

    public static CurrController getInstance(SettingsLayer.Signature sg){
        if(sg == null){
            throw new NullPointerException("Method signature is null!");
        }
        return instance;
    }

// endregion

// region Initialize

    private String searchForLatestBackup() throws NullPointerException{
        // No internet connection, search for older files
        List<String> fileNames = FileUtil.getFileNamesInFolder(new File(GluonApplication.getPlatformPath()+"/dailycurrency/"));
        if(fileNames.size() == 0){
            throw new NullPointerException("No backup file found!");
        }

        return FileUtil.removeExtension(fileNames.get(fileNames.size()-1));
    }

    private void storeData(final Map<String,Double> data) throws IOException {
        Path newFile = Paths.get(GluonApplication.getPlatformPath()+"/dailycurrency/"+latestDate+".ser");
        Files.createDirectories(newFile.getParent());
        if(!Files.exists(newFile)) {
            Files.createFile(newFile);
        }
        if(FileUtil.isFileEmpty(newFile)){
            currencySer = new Serialize<>(newFile);
            currencySer.write(data);
        }
        cd = data;
    }

    private void setDateLabel(){
        String[] dateSplit = latestDate.split("-");
        infoLbl.setText("Exchange rates are provided by: " + LatestCurrDAO.SOURCE_NAME +
                " ("+ dateSplit[2]+"-"+dateSplit[1]+"-"+dateSplit[0] +")");
    }

    private PrevInput initPreviousData() throws IOException, ClassNotFoundException {

        Path prevPath = Paths.get(GluonApplication.getPlatformPath()+"/prevData.ser");
        prevSer = new Serialize<>(prevPath);

        Files.createDirectories(prevPath.getParent());
        if(!Files.exists(prevPath)) {
            Files.createFile(prevPath);
            return new PrevInput(1, 0, 1);
        } else {
            if(FileUtil.isFileEmpty(prevPath)){
                throw new EOFException("File is empty");
            }
            return prevSer.read();
        }

    }

    public boolean isFatalError = false;
    private void initCurrency() throws IOException, ClassNotFoundException, InterruptedException {
        if(!Files.exists(Paths.get(GluonApplication.getPlatformPath() +"/dailycurrency/"+ latestDate + ".ser"))){   // if it is not exist, create one
            Map<String, Double> data;
            try {
                data = LatestCurrDAO.getInstance(signature).getCurrencyData();
                try {
                    storeData(data);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
                setDateLabel();
            } catch (NullPointerException | IllegalAccessException | IOException e) {
                try {
                    latestDate = searchForLatestBackup();
                    try {
                        currencySer = new Serialize<>(Paths.get(GluonApplication.getPlatformPath() +"/dailycurrency/"+ latestDate +".ser"));
                        cd = currencySer.read();
                    } catch (IOException | ClassNotFoundException | NullPointerException e2) {
                        e2.printStackTrace();
                    }
                    setDateLabel();
                } catch (NullPointerException npe){
                    System.out.println("NO BACKUP AND NO INTERNET!!!");
                    isFatalError = true;
                }
            }
        } else {
            setDateLabel();
            currencySer = new Serialize<>(Paths.get(GluonApplication.getPlatformPath() +"/dailycurrency/"+ latestDate + ".ser"));
            // Throws IOException & ClassNotFoundException
            cd = currencySer.read();
        }

    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        try {
            initCurrency();
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }

        if(com.gluonhq.charm.down.Platform.isAndroid()) {
            containerPane.setOnSwipeRight(event -> {
                MobileApplication.getInstance().showLayer(GluonApplication.LEFT_LAYER);
            });
        }

        Image arrowImg = new Image(getClass().getResourceAsStream("/img/reverse_arrow.png"));
        arrowImgVw.setImage(arrowImg);

        PrevInput prevInput = null;
        try {
            prevInput = initPreviousData();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        initCurrencyDropDown();

        if(prevInput == null){
            initInputFields(1.0,0,1);
        } else {
            initInputFields(prevInput.value, prevInput.from, prevInput.to);
        }

        currencyTag.setText(currencyFrom.getSelectedItem().getText().substring(0,3));

        // Digits of length 0-7, digits of length 0-6 after dot, ()? means it occurs only once or not at all
        TextFieldUtil.forceTextFieldToNumeric(amountSpinner.getEditor(), "\\d{0,15}([\\.]\\d{0,6})?");

        amountSpinner.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            SpinnerUtil.handleSpinIfValueIsNull(amountSpinner, oldValue, newValue);
        });

        amountSpinner.getEditor().textProperty().addListener(e -> {
            String val = amountSpinner.getEditor().getText();
            if (MathUtil.isNumber(val)) {
                Platform.runLater( () ->
                    getResult()
                );
            }
        });

        if(com.gluonhq.charm.down.Platform.isDesktop()) {
            addOnActionToMenuItems(currencyTo, currencyFrom);
        } else {
            addTouchToMenuItems();
        }

        reverseCurrency();
        if(cd != null) {
            final PrevInput finalPrevData = prevInput;
            Platform.runLater( () -> getResult(Objects.requireNonNull(finalPrevData).value));
        }

    }

    private void initCurrencyDropDown(){
        List<String> shortName = new ArrayList<>();
        List<String> fullName = new ArrayList<>();
        for (Map.Entry<String, String> entry : CurrencyPairs.CURRENCY_MAP_52.entrySet()){
            shortName.add(entry.getKey());
            fullName.add(entry.getKey()+" - "+entry.getValue());
        }

        DropdownButtonUtil.initDropDownWithImgAndText(shortName, fullName, Config.getFlagIcoDir(),".png", currencyFrom, currencyTo);

    }

    private void initInputFields(double spinVal, int selectedFrom, int selectedTo){
        amountSpinner.getValueFactory().setValue(spinVal);

        currencyFrom.setSelectedItem(selectedFrom);
        currencyTo.setSelectedItem(selectedTo);
    }

// endregion

// region Result

    public void addOnActionToMenuItems(final DropdownButton ... ctrls){
        for (DropdownButton ctrl : ctrls){
            ObservableList<MenuItem> menuItems = ctrl.getItems();
            for (MenuItem m : menuItems){
                m.setOnAction(e -> {
                    getResult();
                    if(ctrl.equals(currencyFrom)){
                        currencyTag.setText(currencyFrom.getSelectedItem().getText().substring(0,3));
                    }
                });
            }
        }
    }

    public void addTouchToMenuItems(final DropdownButton ... ctrls){
        /*for (DropdownButton ctrl : ctrls){

            ObservableList<MenuItem> menuItems = ctrl.getItems();
            for (MenuItem m : menuItems){
                m.onActionProperty().addListener( new TouchEvent (
                    getResult(Double.parseDouble(amountSpinner.getEditor().getText())),
                    if(ctrl.equals(currencyFrom))
                        currencyTag.setText(currencyFrom.getSelectedItem().getText().substring(0,3))

                )

            }
        }*/
    }

    public static StringBuilder decimalFormat = new StringBuilder("#,###");
    public void getResult(final double amount) {
        String fromStr = currencyFrom.getSelectedItem().getText().substring(0,3); //.getSelectionModel().getSelectedItem().substring(0,3);
        String toStr = currencyTo.getSelectedItem().getText().substring(0,3);  //getSelectionModel().getSelectedItem().substring(0,3);

        double toValue = 0, fromValue = 0;
        try {
            fromValue = getCurrencyValue(fromStr);
            toValue = getCurrencyValue(toStr);
        } catch (UnsupportedCurrencyException | NullPointerException myExc) {
            myExc.printStackTrace();
        }

        fromToLbl.setText(MathUtil.roundNumber(amount, decimalFormat.toString()) + " " + fromStr + ":");
        resultLbl.setText(MathUtil.roundNumber(toValue/fromValue*amount, decimalFormat.toString()) + " " + toStr);

    }

    public void getResult() {
        String fromStr = currencyFrom.getSelectedItem().getText().substring(0,3); //.getSelectionModel().getSelectedItem().substring(0,3);
        String toStr = currencyTo.getSelectedItem().getText().substring(0,3);  //getSelectionModel().getSelectedItem().substring(0,3);

        double toValue = 0, fromValue = 0;
        try {
            fromValue = getCurrencyValue(fromStr);
            toValue = getCurrencyValue(toStr);
        } catch (UnsupportedCurrencyException | NullPointerException myExc) {
            myExc.printStackTrace();
        }

        double val = Double.parseDouble(amountSpinner.getEditor().getText());

        fromToLbl.setText(MathUtil.roundNumber(val, decimalFormat.toString()) + " " + fromStr + ":");
        resultLbl.setText(MathUtil.roundNumber(toValue / fromValue * val, decimalFormat.toString()) + " " + toStr);

    }

    private double getCurrencyValue(final String currency) throws UnsupportedCurrencyException, NullPointerException {
        if(cd == null){
            throw new NullPointerException("CurrencyValuePair ArrayList is null");
        }

        for (Map.Entry<String,Double> entry : cd.entrySet()) {
            if(entry.getKey().equals(currency)){
                return entry.getValue();
            }
        }

        throw new UnsupportedCurrencyException("Currency type: "+ currency +" is not supported!");
    }

    /* 2---- ARROW THAT REVERSES FROM AND TO CURRENCY ---- */
    public void reverseCurrency() {
        arrowImgVw.setOnMouseClicked(mouseEvent -> {
            if(amountSpinner.getEditor().getText().isEmpty()){
                amountSpinner.getEditor().setText("1");
            }

            final int temp = currencyFrom.getSelectedIndex();
            currencyFrom.setSelectedItem(currencyTo.getSelectedIndex());
            currencyTo.setSelectedItem(temp);

            getResult();
            currencyTag.setText(currencyFrom.getSelectedItem().getText().substring(0,3));
        });
    }

// endregion

// region CloseApp

    public void storePrevData() throws IOException {
        double validValue;
        if(amountSpinner.getEditor().getText().isEmpty()){
            validValue = amountSpinner.getValue();
        } else {
            validValue = Double.parseDouble(amountSpinner.getEditor().getText());
        }

        if(prevSer == null){
            prevSer = new Serialize<>(Paths.get(GluonApplication.getPlatformPath()+"/prevData.ser"));
        }
        prevSer.write(new PrevInput(validValue, currencyFrom.getSelectedIndex(), currencyTo.getSelectedIndex()));

    }

    public void closeApplication(){
        GluonApplication.closeApp();
    }

// endregion

}
