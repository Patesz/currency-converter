package hu.pte.dhizri.controller;

import com.gluonhq.charm.glisten.control.DropdownButton;
import com.gluonhq.charm.glisten.layout.layer.SidePopupView;
import com.gluonhq.charm.glisten.visual.Swatch;
import hu.pte.dhizri.GluonApplication;
import hu.pte.dhizri.model.Serialize;
import hu.pte.dhizri.my.MyDropdownButton;
import hu.pte.dhizri.util.fx.DropdownButtonUtil;
import hu.pte.dhizri.util.java.FileUtil;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

final class Color {

    public static List<String> colors = new ArrayList<String>() {{
        add("BLUE");
        add("BROWN");
        add("GREEN");
        add("ORANGE");
        add("RED");
    }};

}

final class Toggle{

    private static SimpleBooleanProperty switchedOn = new SimpleBooleanProperty(false);

    protected static void setIsOn(boolean setState){
        switchedOn = new SimpleBooleanProperty(setState);
    }

    public static boolean getIsOn(){
        return switchedOn.get();
    }

    public static void addToggleEvent(StackPane sp, Button btn, Image onImg, Image offImg) {

        btn.setOnAction(e -> {
            switchedOn.set(!switchedOn.get());
            if (switchedOn.get()) {
                btn.setText("On");
                sp.setStyle("-fx-background-color: rgba(51,51,51,0.85); -fx-padding: 10;");

                GluonApplication.getScene().getStylesheets().remove("/css/dark/WHITE.css");
                GluonApplication.getScene().getStylesheets().add("/css/dark/DARK.css");

                btn.setGraphic(new ImageView(onImg));
            } else {
                btn.setText("Off");
                sp.setStyle("-fx-background-color: rgba(255,255,255,0.85); -fx-padding: 10;");

                GluonApplication.getScene().getStylesheets().remove("/css/dark/DARK.css");
                GluonApplication.getScene().getStylesheets().add("/css/dark/WHITE.css");

                btn.setGraphic(new ImageView(offImg));
            }
        });
    }
}

final class PrevTheme implements Serializable {
    private static final long serialVersionUID = 3L;

    public static final String filePath = GluonApplication.getPlatformPath()+"/prevTheme.ser";

    public String prevColor;
    public boolean prevDark;
    public int prevDigit;

    public PrevTheme(String col, boolean dark, int digit){
        this.prevColor = col;
        this.prevDark = dark;
        this.prevDigit = digit;
    }

}

public final class SettingsLayer implements Initializable {

    @FXML private SidePopupView sidePopup;
    @FXML private StackPane stackPane;
    @FXML private Slider digitSlider;
    @FXML private Label digitLabel;

    @FXML private MyDropdownButton colorPicker;
    @FXML private Button darkToggle;

    private static Serialize<PrevTheme> serializedTheme = null;
    private static Path prevPath = null;

    private static SettingsLayer instance;

    public SettingsLayer(){
        instance = this;
    }

    private static final Signature signature = new Signature();

    public static class Signature {
        private Signature() {}
    }

    public static SettingsLayer getInstance(GluonApplication.Signature sg){
        if(sg == null){
            throw new NullPointerException("Method signature is null!");
        }
        return instance;
    }


// region Initialize

    public void restorePreviousTheme() throws IOException, ClassNotFoundException {

        Files.createDirectories(prevPath.getParent());
        if(!Files.exists(prevPath)) {
            Files.createFile(prevPath);
            GluonApplication.getScene().getStylesheets().add("/css/dark/WHITE.css");
            GluonApplication.getScene().getStylesheets().add("/css/color/BLUE.css");
        } else {
            if(FileUtil.isFileEmpty(prevPath)){
                throw new EOFException("File is empty");
            }
            PrevTheme pv = serializedTheme.read();

            if(pv.prevDark) {
                GluonApplication.getScene().getStylesheets().add("/css/dark/DARK.css");
            } else{
                GluonApplication.getScene().getStylesheets().add("/css/dark/WHITE.css");
            }

            GluonApplication.getScene().getStylesheets().add("/css/color/"+pv.prevColor+".css");

            Swatch colorSwatch = Swatch.valueOf(pv.prevColor);
            colorSwatch.assignTo(GluonApplication.getScene());
        }
    }

    public void storeTheme() throws IOException {
        serializedTheme.write(new PrevTheme(getActiveColorTheme(), getIsDarkTheme(), digitSlider.valueProperty().intValue()));
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        prevPath = Paths.get(PrevTheme.filePath);
        serializedTheme = new Serialize<>(prevPath);

        try {
            PrevTheme pv = serializedTheme.read();
            if(pv.prevDark) stackPane.setStyle("-fx-padding: 10; -fx-background-color: rgba(51,51,51,0.9);");
            else stackPane.setStyle("-fx-padding: 10; -fx-background-color: rgba(255,255,255,0.9);");
            digitSlider.valueProperty().setValue(pv.prevDigit);
            this.getResultByDecimalDigit(pv.prevDigit);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        digitSlider.valueProperty().addListener((observable, oldvalue, newvalue) ->
        {
            int val = newvalue.intValue();
            digitLabel.setText(Integer.toString(val));
            Platform.runLater(() -> getResultByDecimalDigit(val));
        });

    }

    public void getResultByDecimalDigit(int val){
       try {
            CurrController.decimalFormat.delete(4, CurrController.decimalFormat.length()-1);
            for (int i = 0; i < val; i++) {
                if(i==0) {
                    CurrController.decimalFormat.append(".");
                }
                CurrController.decimalFormat.append("#");
            }
            CurrController.getInstance(signature).getResult();
        } catch (NumberFormatException nfe) {
            nfe.getMessage();
        }
    }

    public void initStyle(){
        try {
            restorePreviousTheme();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        Toggle.addToggleEvent(stackPane, darkToggle, new Image("/img/colors/MOON.png"), new Image("/img/colors/SUN.png"));

        initColorDropDown();
    }

    private void initColorDropDown(){
        DropdownButtonUtil.initDropDownWithImgAndText(Color.colors, Color.colors, "/img/colors/", ".png", colorPicker);
        addColorEvent(colorPicker);
    }

    private void addColorEvent(DropdownButton ddb){
        ObservableList<MenuItem> menuItems = ddb.getItems();

        int i=0;
        for (MenuItem mi : menuItems){
            final int finalI = i;
            mi.setOnAction(e->{
                Swatch colorSwatch = Swatch.valueOf(Color.colors.get(finalI));
                colorSwatch.assignTo(GluonApplication.getScene());

                for (String iColor : Color.colors) {
                    GluonApplication.getScene().getStylesheets().remove("/css/color/"+iColor+".css");
                }

                ddb.setStyle("-fx-border-color: "+ colorSwatch +"; -fx-border-width: 0 0 3 0;");
                GluonApplication.getScene().getStylesheets().add("/css/color/"+colorSwatch+".css");
            });
            i++;
        }
    }

// endregion

    public String getActiveColorTheme(){
        for (String iColor : Color.colors) {
            if(GluonApplication.getScene().getStylesheets().contains("/css/color/"+iColor+".css")) {
                return iColor;
            }
        }
        return null;
    }

    public String getSelectedMenuItem(){
        for (String iColor : Color.colors) {
            if(GluonApplication.getScene().getStylesheets().contains("/css/color/"+iColor+".css")) {
                return iColor;
            }
        }
        return null;
    }

    public boolean getIsDarkTheme(){
        return !GluonApplication.getScene().getStylesheets().contains("/css/dark/WHITE.css");
    }

    public void setLayerPrevState() {
        if(getIsDarkTheme()){
            darkToggle.setText("On");
            darkToggle.setGraphic(new ImageView(new Image("/img/colors/MOON.png")));
            Toggle.setIsOn(true);
        } else {
            darkToggle.setText("Off");
            darkToggle.setGraphic(new ImageView(new Image("/img/colors/SUN.png")));
            Toggle.setIsOn(false);
        }

        selectActiveColorTheme();
    }

    private void selectActiveColorTheme(){
        String currentColor = getActiveColorTheme();
        for (MenuItem mi : colorPicker.getItems()){
            if(mi.getText().equals(currentColor)){
                colorPicker.setSelectedItem(mi);
            }
        }
    }

}
