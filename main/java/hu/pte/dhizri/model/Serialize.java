package hu.pte.dhizri.model;


import java.io.*;
import java.nio.file.Path;

public final class Serialize<T>{

    public Path filePath;

    public Serialize(Path filePath){
        this.filePath = filePath;
    }

    public void write(T obj) throws IOException {
        FileOutputStream fout = new FileOutputStream(filePath.toFile());
        ObjectOutputStream out = new ObjectOutputStream(fout);
        out.writeObject(obj);
        out.close();
        fout.close();
    }

    public T read() throws IOException, ClassNotFoundException, NullPointerException {
        FileInputStream fIn = new FileInputStream(filePath.toFile());
        ObjectInputStream in = new ObjectInputStream(fIn);
        T obj = (T) in.readObject();
        if(obj == null){
            throw new NullPointerException("Getting object from "+ filePath.toFile() +" returned null!");
        }
        fIn.close();
        in.close();
        return obj;
    }

}
