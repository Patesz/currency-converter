package hu.pte.dhizri.model;

import hu.pte.dhizri.controller.CurrController;
import hu.pte.dhizri.util.fx.AlertUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

//import com.fasterxml.jackson.

public class LatestCurrDAO {
    private static final String SOURCE_URL = "https://api.exchangerate-api.com/v4/latest/USD";
    public static final String SOURCE_NAME = "exchangerate-api";

    private static LatestCurrDAO currencyUpdater = null;
    //private static int sourceIndex = 0;

    private LatestCurrDAO() { }

// region Getters

    // Singleton - create instance if not exists, else return the existing one
    public static LatestCurrDAO getInstance(CurrController.Signature signature) throws IllegalAccessException {
        if(signature == null){
            throw new IllegalAccessException("CurrencyController's signature is required for CurrUpdater!");
        }

        if (currencyUpdater == null) {
            currencyUpdater = new LatestCurrDAO();
        }
        return currencyUpdater;
    }

// endregion

// region GetData

    public Map<String, Double> getCurrencyData() throws IOException, NullPointerException {
        URL webUrl = null;

        //sets the web url according to the index of the selected source
        //Throws MalformedUrlException
        webUrl = new URL(SOURCE_URL);

        if (webUrl == null) {
            throw new NullPointerException("webUrl variable is null");
        }

        //Throws UnknownHostException
        InputStream stream = webUrl.openStream();

        // Throws IOException
        String rawData = new BufferedReader(new InputStreamReader(stream)).readLine();

        Map<String, Double> back = processData(rawData);

        return back;
    }

    /**
     * function is called in CurrencyController getResult() method - base currency USD
     * @return splitted and shortened (e.g. USD) currency content as an ArrayList
     * @throws NullPointerException - if content is null
     */
    public Map<String,Double> processData(String rawData) throws NullPointerException {
        if (rawData == null) {
            AlertUtil.showError("Data is not downloaded from source.",null);
            AlertUtil.showInfo("Check your internet connection.","Connection.");
            throw new NullPointerException("Buffered content variable is null.");
        }
        Map<String,Double> back = new TreeMap<>();

        String[] mainParts = rawData.split(Pattern.quote("{"));
        String[] currencyPairs = mainParts[2].split(",");

        // only loops to last-1 element
        for (int i = 0; i < currencyPairs.length - 1; ++i) {
            String[] currAndVal = currencyPairs[i].split(":");
			/*  Adding shortened currency type and it's value (base currency value: USD) and
			    creating new CurrencyData    */
			back.put(currAndVal[0].substring(1, 4), Double.parseDouble(currAndVal[1]));
        }

        // last currency contains unwanted characters, so remove it
        String[] currAndVal = currencyPairs[currencyPairs.length - 1].split(":");

        back.put(currAndVal[0].substring(1, 4),
                Double.parseDouble(currAndVal[1].substring(0, currAndVal[1].length() - 2)));

        return back;
    }

// endregion

}
