package hu.pte.dhizri.model;

import java.util.Map;
import java.util.TreeMap;

public final class CurrencyPairs {

    private CurrencyPairs() {}

    public static final Map<String, String> CURRENCY_MAP_33 = new TreeMap<String, String>() {{
        put("AUD", "Australian Dollar"); put("BGN", "Bulgarian Lev"); put("BRL", "Brazilian Real");
        put("CAD", "Canadian Dollar"); put("CHF", "Swiss Franc"); put("CNY", "Chinese Yuan");
        put("CZK", "Czech Koruna"); put("DKK", "Danish Krone"); put("EUR", "Euro");
        put("GBP", "British Pound"); put("HKD", "Hong Kong Dollar"); put("HRK", "Croation Kuna");
        put("HUF", "Hungarian Forint"); put("IDR", "Indonesian Rupiah"); put("ILS", "Israeli Shekel");
        put("INR", "Indian Rupee"); put("ISK", "Icelandic Krona"); put("JPY", "Japanese Yen");
        put("KRW", "South Korean Won"); put("MXN", "Mexican Peso"); put("MYR", "Malaysian Ringgit");
        put("NOK", "Norwegian Krone"); put("NZD", "New Zealand Dollar"); put("PHP", "Philippine Peso");
        put("PLN", "Polish złoty"); put("RON", "Romanian Leu"); put("RUB", "Russian Ruble");
        put("SEK", "Swedish Krona"); put("SGD", "Singapore Dollar"); put("THB", "Thai Baht");
        put("TRY", "Turkish Lira"); put("USD", "US Dollar"); put("ZAR", "South African Rand");
    }};

    public static final Map<String, String> CURRENCY_MAP_52 = new TreeMap<String, String>() {{
        put("AED", "UEA Dirham"); put("ARS", "Argentine Peso"); put("AUD", "Australian Dollar");
        put("BGN", "Bulgarian Lev"); put("BRL", "Brazilian Real"); put("BSD", "Bahamian Dollar");
        put("CAD", "Canadian Dollar"); put("CHF", "Swiss Franc"); put("CLP", "Chilean Peso");
        put("CNY", "Chinese Yuan"); put("COP", "Colombian Peso"); put("CZK", "Czech Koruna");
        put("DKK", "Danish Krone"); put("DOP", "Dominican Peso"); put("EGP", "Egyption Pound");
        put("EUR", "Euro"); put("FJD", "Fijian Dollar"); put("GBP", "British Pound");
        put("GTQ", "Guatemalan Quetzal"); put("HKD", "Hong Kong Dollar"); put("HRK", "Croation Kuna");
        put("HUF", "Hungarian Forint"); put("IDR", "Indonesian Rupiah"); put("ILS", "Israeli Shekel");
        put("INR", "Indian Rupee"); put("ISK", "Icelandic Krona"); put("JPY", "Japanese Yen");
        put("KRW", "South Korean Won"); put("KZT", "Kazakhstani Tenge"); put("MXN", "Mexican Peso");
        put("MYR", "Malaysian Ringgit"); put("NOK", "Norwegian Krone"); put("NZD", "New Zealand Dollar");
        put("PAB", "Panamanian Balboa"); put("PEN", "Peruvian Sol"); put("PHP", "Philippine Peso");
        put("PKR", "Pakistani Rupee"); put("PLN", "Polish złoty"); put("PYG", "Paraguayan Guarani");
        put("RON", "Romanian Leu"); put("RUB", "Russian Ruble"); put("SAR", "Saudi Arabian Riyal");
        put("SEK", "Swedish Krona"); put("SGD", "Singapore Dollar"); put("THB", "Thai Baht");
        put("TRY", "Turkish Lira"); put("TWD", "Taiwan New Dollar"); put("UAH", "Ukrainian Hryvnia");
        put("USD", "US Dollar"); put("UYU", "Uruguay Peso"); put("VND", "Vietnamese Dong");
        put("ZAR", "South African Rand");
    }};


}
