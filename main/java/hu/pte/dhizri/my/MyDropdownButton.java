package hu.pte.dhizri.my;

import com.gluonhq.charm.glisten.control.DropdownButton;
import javafx.scene.control.MenuItem;

public class MyDropdownButton extends DropdownButton {

    public int getSelectedIndex() throws NullPointerException {

        MenuItem selected = super.getSelectedItem();

        int i = 0;
        for (MenuItem m : super.getItems()) {
            if (m.equals(selected)) {
                return i;
            }
            i++;
        }

        throw new NullPointerException("No selected item");
    }

    public void setSelectedItem(int index) throws IndexOutOfBoundsException {

        int i = 0;
        for (MenuItem m : super.getItems()) {
            if (i == index) {
                super.setSelectedItem(m);
                return;
            }
            i++;
        }

        throw new IndexOutOfBoundsException("Index value is bigger then the object size");
    }

}
