package hu.pte.dhizri.util.java;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

public final class MathUtil{

	private MathUtil(){}

	public static class Stat {
		public static double avg(final Collection<Double> data) {
			Double sum = 0.0;
			int size = data.size();
			for (Double num : data) {
				sum += num;
			}
			
			return sum/size;
		}
		
		public static double stdDev(final double avg ,final Collection<Double> arrayList) {
			double standardDeviation = 0.0;
			
			for (Double num: arrayList) {
				standardDeviation += Math.pow(num - avg, 2);
			}
			
			return Math.sqrt(standardDeviation/arrayList.size());
		}
	}
	
	public static String roundNumber(final Number num, final String decimalFormat) {
		DecimalFormat df = new DecimalFormat(decimalFormat);
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		return df.format(num);
	}

	public static boolean isNumber(final String str) throws NullPointerException {
		if(str.length() > 0){
			int i=0;

			if(str.charAt(0) == '-' || str.charAt(0) == '+')
				++i;
			if(i==1 && str.length() == 1)
				return false;

			boolean isDotCharFound = false;
			for (; i < str.length(); ++i) {
				if(!Character.isDigit(str.charAt(i))) {
					if(str.charAt(i) == '.' && !isDotCharFound && (i != 0 || str.length() != 1)){
						isDotCharFound = true;
					} else {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}

	public static double min(final List<Double> valueList){
		double min = Double.MAX_VALUE;
		for (Double val : valueList) {
			if (val < min)
				min = val;
		}
		return min;
	}

	public static int maxInt(final List<Integer> valueList){
		int max = Integer.MIN_VALUE;
		for (int val : valueList) {
			if (val > max)
				max = val;
		}
		return max;
	}

	public static double max(final List<Double> valueList){
		double max = Double.MIN_VALUE;
		for (double val : valueList) {
			if (val > max)
				max = val;
		}
		return max;
	}

	public static double min(final double[] values){
		double min = Double.MAX_VALUE;
		for (double val : values) {
			if (val < min)
				min = val;
		}
		return min;
	}

	public static double max(final double[] values){
		double max = Double.MIN_VALUE;
		for (double val : values) {
			if (val > max)
				max = val;
		}
		return max;
	}

}