package hu.pte.dhizri.util.java;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class FileUtil{

    private FileUtil() {}

    /*public static String getExtension(final String fileName) throws NoFileExtensionException {
        // "\\." means to split by dot
        // "." would mean split by any character
        String[] splittedFileName = fileName.split("\\.");
        if(splittedFileName.length <= 1){
            final String errorMsg = "String parameter has no extension!";
            throw new NoFileExtensionException(errorMsg);
        }

        return splittedFileName[splittedFileName.length-1];
    }*/

    public static String removeExtension (String fileName){
        String[] spl = fileName.split("\\.");
        return spl[0];
    }

    /*public static boolean isFileExists(URL urlPath){
        if(urlPath == null){
            return false;
        }

        String strPath = FileUtil.removeFileStrFromURL(urlPath);
        File f = new File(strPath);
        if(f.isFile() && !f.isDirectory()) {
            return true;
        }
        return false;
    }*/

    public static boolean isFileExists(String strPath){
        if(strPath == null){
            return false;
        }

        File f = new File(strPath);
        if(f.isFile() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public static String removeFileStrFromURL(URL urlPath){
        String path = String.valueOf(urlPath);
        final String remove = "file:/";
        if(path.contains(remove)){
            return path.substring(remove.length());
        }
        return path;
    }

    public static List<String> getFileNamesInFolder(final File folder) {
        List<String> fileNames = new ArrayList<>();

        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                getFileNamesInFolder(fileEntry);
            } else {
                System.out.println(fileEntry);
                fileNames.add(fileEntry.getName());
            }
        }
        return fileNames;
    }

    public static List<String> readLines(String filePath) throws IOException {
        // Character coding can be an issue is some cases.
        return Files.readAllLines(Paths.get(filePath), StandardCharsets.ISO_8859_1);
    }

    public static boolean isFileEmpty(Path fullPath) throws IOException {
        if(Files.exists(fullPath) && Files.size(fullPath) <= 0 && !Files.isDirectory(fullPath)){
            return true;
        }
        return false;
    }

    /*/**
     * @param addContent - String to add in those rows, where rowsToUpdate is found.
     * @param rowsToUpdate - Search for rows in file with these value(s).

    public static List<String> getUpdatedContent(List<String> rowList , String addContent, String... rowsToUpdate){
        for(int i = 0; i < rowsToUpdate.length; ++i){
            for(int j = 0; j < rowList.size(); ++j){
                if(rowList.get(j).contains(rowsToUpdate[i])){
                    rowList.remove(j);
                    rowList.add(j,rowsToUpdate[i] + addContent);
                }
            }
        }
        return rowList;
    }*/

    public static void delete(String filePath) throws IOException {
        Files.delete(Paths.get(filePath));
    }

    /**
     * @param filePath
     * @param rows
     * @throws IOException
     */
    public static void create(String filePath, ArrayList<String> rows) throws IOException {
        Files.createFile(Paths.get(filePath));
        Files.write(Paths.get(filePath), rows);
    }

    // -1: failed to create file, 0: just now created, 1: already exists
    public static int createFileIfNotExist(String sourcePath, String fileName) throws IOException {

        if(!FileUtil.isFileExists(sourcePath + fileName)){
            boolean isCreated = new File(sourcePath+fileName).createNewFile();
            if(!isCreated){
                return -1;
            }
            return 0;
        }

        return 1;
    }


}
