package hu.pte.dhizri.util.fx;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DateUtil {

    private DateUtil() {}

    public static String getToday(String format){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format); // yyyy-MM-dd
        LocalDate now = LocalDate.now();
        return dtf.format(now);
    }

    public static String getNow(String format){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format); // yyyy-MM-dd
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

}
