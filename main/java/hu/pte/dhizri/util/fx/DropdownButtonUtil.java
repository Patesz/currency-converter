package hu.pte.dhizri.util.fx;

import com.gluonhq.charm.glisten.control.DropdownButton;
import com.gluonhq.charm.glisten.visual.Swatch;
import hu.pte.dhizri.Config;
import hu.pte.dhizri.model.CurrencyPairs;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;
import java.util.Map;

public final class DropdownButtonUtil {

	private DropdownButtonUtil(){}

	/*public static void initDropDownWithImgAndText(DropdownButton ... dbs) {

		for (Map.Entry<String, String> entry : CurrencyPairs.CURRENCY_MAP_52.entrySet()) {

			Image img = new Image(Config.getFlagIcoDir() + entry.getKey() + ".png");
			String text = entry.getKey() + " - " + entry.getValue();
			MenuItem[] m = new MenuItem[dbs.length];

			for (int i = 0; i < dbs.length; i++) {
				ImageView imgView = new ImageView(img);
				m[i] = new MenuItem(text);
				m[i].setGraphic(imgView);
				dbs[i].getItems().add(m[i]);
			}
		}

	}*/


	public static void initDropDownWithImgAndText(List<String> imageNameList, List<String> textNameList, String imgDir, String imgFormat, DropdownButton ... dbs) {

		int textListIndex = 0;
		for (String str : imageNameList) {
			Image img = new Image(imgDir + str + imgFormat);
			MenuItem[] m = new MenuItem[dbs.length];

			for (int i = 0; i < dbs.length; i++) {
				ImageView imgView = new ImageView(img);
				m[i] = new MenuItem(textNameList.get(textListIndex));
				m[i].setGraphic(imgView);
				dbs[i].getItems().add(m[i]);
			}

			textListIndex++;
		}
	}




}
