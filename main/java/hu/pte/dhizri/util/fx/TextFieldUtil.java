package hu.pte.dhizri.util.fx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public final class TextFieldUtil {

	private TextFieldUtil(){}

	/* 3--- RESTRICT SPINNER LENGTH AND NUMERIC FORMAT --- */
	public static void forceTextFieldToNumeric(final TextField tf, String regex) {
		tf.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(final ObservableValue<? extends String> observable, String oldValue, String newValue) {

		    	if (!newValue.matches(regex)) {
		            tf.setText(oldValue);
		        }
		    }
		});
	}
	
	
	public static void textLengthLimiter(final TextField tf, final int maxLength) {
	   	tf.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
	            if (tf.getText().length() > maxLength) {
	                String s = tf.getText().substring(0, maxLength);
	                tf.setText(s);
	            }
	        }
	    });
	}
}
