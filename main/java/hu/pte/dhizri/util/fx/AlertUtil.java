package hu.pte.dhizri.util.fx;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/*
The class should contain all static methods and should not be declared abstract
(as that would imply the class is not concrete and has to be implemented in some way).
*/

// Can't be extended because the class is final
public final class AlertUtil {

	private AlertUtil(){}

	// shows an error type panel
	public static void showError(String contentText, String headerText) {
		Alert error = new Alert(AlertType.ERROR);
		error.setTitle("An error occured!");
		if(headerText != null) {
			error.setHeaderText(headerText);
		}
		error.setContentText(contentText);
		error.showAndWait();
	}
	
	// shows a warning type panel
	public static void showWarning(String contentText) {
		Alert warning = new Alert(AlertType.WARNING);
		warning.setTitle("Warning!");
		warning.setContentText(contentText);
		warning.showAndWait();
	}
	
	// shows confirm panel with an Ok and Cancel button
	public static boolean showConfirm(String contentText) {
		Alert confirm = new Alert(AlertType.CONFIRMATION);
		confirm.setTitle("Confirm operation?");
		confirm.setContentText(contentText);
		Optional<ButtonType> result = confirm.showAndWait();
		
		if(result.get() != ButtonType.OK) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static void showInfo(String contentText, String headerText) {
		Alert info = new Alert(AlertType.INFORMATION);
		info.setTitle("Information.");
		if(headerText != null) {
			info.setHeaderText(headerText);
		}
		info.setContentText(contentText);
		info.showAndWait();
	}

}
