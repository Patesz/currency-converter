package hu.pte.dhizri.util.fx;

import javafx.scene.control.Control;

public final class ControlUtil {
    private ControlUtil() {}

    public static void setControlSize(Double minWidth, Double maxWidth, Double minHeight, Double maxHeight, Control... ctrls){
        for (Control ctrl : ctrls) {
            if (minWidth != null)
                ctrl.setMinWidth(minWidth);
            if (maxWidth != null)
                ctrl.setMaxWidth(maxWidth);
            if (minHeight != null)
                ctrl.setMinHeight(minHeight);
            if (maxHeight != null)
                ctrl.setMaxHeight(maxHeight);
        }
    }

}
