package hu.pte.dhizri.exc;

public final class NoFileExtensionException extends Exception {
    public NoFileExtensionException(String message){ super(message); }
}
