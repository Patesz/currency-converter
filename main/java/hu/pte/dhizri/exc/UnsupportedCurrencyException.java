package hu.pte.dhizri.exc;

public final class UnsupportedCurrencyException extends Exception {
    public UnsupportedCurrencyException(String message) {
        super(message);
    }
}
