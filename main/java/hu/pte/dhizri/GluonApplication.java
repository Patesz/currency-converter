package hu.pte.dhizri;

import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Alert;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.layout.layer.SidePopupView;

import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import hu.pte.dhizri.controller.CurrController;

import hu.pte.dhizri.controller.SettingsLayer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GluonApplication extends MobileApplication {

    public static String LEFT_LAYER = "LeftLayer";

    private static Scene scene;

    public static Scene getScene(){
        return scene;
    }

    protected static final Signature signature = new Signature();
    public static class Signature {
        private Signature() {}
    }

    // Glisten constructs an instance of the Application class
    // Then calls init() method
    @Override
    public void init() {

        // System.setProperty("com.gluonhq.charm.down.debug", "true");
        // Initializes project with a GlassPane container (root: AppBar and View)
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CurrencyView.fxml"));
            AnchorPane root = loader.load();

            addViewFactory(HOME_VIEW, () -> new View(root) {

                @Override
                protected void updateAppBar(AppBar appBar) {
                    appBar.setNavIcon(MaterialDesignIcon.MENU.button(
                        e -> MobileApplication.getInstance().showLayer(GluonApplication.LEFT_LAYER))
                    );
                    appBar.setTitleText("Currency Converter");
                    appBar.getActionItems().addAll(
                        MaterialDesignIcon.SEARCH.button(e -> System.out.println("Search")),
                        MaterialDesignIcon.FAVORITE.button(e -> System.out.println("Fav"))
                    );

                    appBar.getMenuItems().add(new MenuItem("Settings"));

                    // View not AppBar

                    /*Close c = new Close();

                    setOnHiding( e->{
                            Services.get(this.getClass()).ifPresent(Services.get(service -> new Close().closeApp()));
                    });*/
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SettingsLayer.fxml"));
            SidePopupView root = loader.load();
            addLayerFactory(LEFT_LAYER, () -> root);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void setProgressBarVisible(boolean setVisible) {
        MobileApplication.getInstance().getAppBar().setProgressBarVisible(setVisible);
    }

    // postInit() is used for one time initialization
    // It runs before displaying the View (pre-setup)
    @Override
    public void postInit(Scene scene) {
        setPlatformUserHome();

        GluonApplication.scene = scene;
        Stage stage = ((Stage) scene.getWindow());
        SettingsLayer.getInstance(GluonApplication.signature).initStyle();
        SettingsLayer.getInstance(GluonApplication.signature).setLayerPrevState();

        // Initialize style after scene is created because it edits home view's style
        // SettingsLayer.getInstance(signature).initLayerStyle();

        if (com.gluonhq.charm.down.Platform.isDesktop()) {
            scene.getWindow().setWidth(390);
            scene.getWindow().setHeight(600);

            stage.setMaxWidth(400);
            stage.setMaxHeight(600);

            stage.setMinWidth(390);
            stage.setMinHeight(500);
        }

        stage.getIcons().add(new Image(GluonApplication.class.getResourceAsStream("/img/bar-chart-ico.png")));
        stage.setTitle("Currency Converter");

        scene.getStylesheets().add("/css/application.css");

        if(CurrController.getInstance(signature).isFatalError){
           showFatalError();
        }

    }

    private void showFatalError(){
        javafx.application.Platform.runLater(() ->{

            Alert error = new Alert(javafx.scene.control.Alert.AlertType.ERROR);

            error.setTitleText("Error: No data is available!");
            error.setContentText("Connect to the internet to get latest data");

            error.setOnCloseRequest(dialogEvent -> System.exit(1));
            error.showAndWait();

            try {
                Thread.sleep(Long.MAX_VALUE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
    }

    public static Path getPlatformPath(){
        if(Platform.isAndroid()){
            return Paths.get( "/storage/emulated/0/hu.ricky.currencyconverter/");
        }
        return Paths.get(System.getProperty("user.home"), ".currencyconverter/");
    }

    private void setPlatformUserHome() {
        if (Platform.isAndroid())
            System.setProperty("user.home", "/storage/emulated/0");
    }

    public static void closeApp() {
        try {
            CurrController.getInstance(GluonApplication.signature).storePrevData();
            SettingsLayer.getInstance(GluonApplication.signature).storeTheme();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MobileApplication.getInstance().getView().fireEvent(
                new WindowEvent(
                        GluonApplication.getScene().getWindow(),
                        WindowEvent.WINDOW_CLOSE_REQUEST
                )
        );

        javafx.application.Platform.exit();
        System.exit(0);

    }

    @Override
    public void stop(){
        closeApp();
    }

}
/*
class CloseApp implements Close {

    @Override
    public void closeApp() {
            try {
                CurrController.getInstance(GluonApplication.signature).storePrevData();
                SettingsLayer.getInstance(GluonApplication.signature).storeTheme();
            } catch (IOException e) {
                e.printStackTrace();
            }

            MobileApplication.getInstance().getView().fireEvent(
                    new WindowEvent(
                            GluonApplication.getScene().getWindow(),
                            WindowEvent.WINDOW_CLOSE_REQUEST
                    )
            );

            javafx.application.Platform.exit();
            System.exit(0);

    }
}
*/