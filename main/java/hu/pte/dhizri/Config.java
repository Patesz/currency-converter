package hu.pte.dhizri;

import hu.pte.dhizri.util.java.FileUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public final class Config {

    /************ !!! Default CONFIG VARIABLES !!! ***********/
    private static String flagIcoDir = "img/countryflags30x30/";
    private static String imgSize = "30";
    private static String color = "#0093ff";
    /*************************************************/

    private final String CONFIG_PATH = FileUtil.removeFileStrFromURL(
            getClass().getResource("/program.cfg"));

    private HashMap<String, String> configPairs = new HashMap<>();

    private static Config instance = null;

    public static Config getInstance() throws IOException {
        if(instance == null) {
            instance = new Config();
        }
        return instance;
    }

    // region Instance
    private Config() throws IOException {
        readConfig();
        readImageSize();
        readColor();
        readCurrencyImageDir();
    }

// endregion

    // region Getters
    public String getConfigPath(){ return CONFIG_PATH; }
    public static String getFlagIcoDir(){ return flagIcoDir; }
    public static String getImgSize(){ return imgSize; }
    public static String getColor(){ return color; }

    /**
     * @param KEY - represents a key in config file
     * @return - returns a value by key param
     */
    private String getMapValue(final String KEY) {
        if (configPairs.containsKey(KEY)) {
            return configPairs.get(KEY);
        }
        return null;
    }
// endregion

    // region Read Config
    private void readConfig() throws IOException {
        List<String> rows = FileUtil.readLines(CONFIG_PATH);

        for (String row : rows){
            String[] rowPairs = row.split(" ");
            configPairs.put(rowPairs[0], rowPairs[1]);
        }
    }

    private void readColor(){
        String color = getMapValue("color");
        if(color != null){
            Config.color = color;
        }
    }

    private void readImageSize(){
        String imgSize = getMapValue("image_size");
        if(imgSize != null){
            Config.imgSize = imgSize;
        }
    }

    private void readCurrencyImageDir(){
        String imageDir = getMapValue("flags_img_path");
        if(imageDir != null){
            Config.flagIcoDir = imageDir;
        }
    }

// endregion

}
