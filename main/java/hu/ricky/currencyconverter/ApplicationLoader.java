package hu.ricky.currencyconverter;

import hu.ricky.currencyconverter.controller.MainController;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public final class ApplicationLoader extends Application {

    private static Scene primaryScene;
    public static Scene getScene(){ return primaryScene; }

    private static final Signature signature = new Signature();
    public static class Signature { private Signature() {} }

    private MainController mainController = null;

    @Override
    public void start(Stage stage) {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainView.fxml"));

        try {
            Parent root = loader.load();
            primaryScene = new Scene(root);
            primaryScene.getStylesheets().add("/css/application.css");
            stage.setScene(primaryScene);

            mainController = loader.getController();

            mainController.setDateLbl();
            mainController.settingsController.init();
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.setWidth(408);
        stage.setHeight(637);

        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        stage.setX((screenBounds.getMaxX()/2) - (stage.getWidth()/2));
        stage.setY(screenBounds.getMinY());

        stage.setResizable(false);

        stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/dollar.png")));
        stage.setTitle("Currency Converter");

        stage.show();
    }

    @Override
    public void stop(){
        try {
            mainController.storeAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Platform.exit();
        System.exit(0);
    }

    public static void main(String[] args) {

        try {
            User.setPrivateStorage();
        } catch (IOException e) {
            e.printStackTrace();
        }

        launch(args);
    }

}
