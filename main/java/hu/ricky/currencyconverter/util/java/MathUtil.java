package hu.ricky.currencyconverter.util.java;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public final class MathUtil{

	private MathUtil(){}
	
	public static String roundNumber(final Number num, final String decimalFormat) {
		DecimalFormat df = new DecimalFormat(decimalFormat);
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		return df.format(num);
	}

}