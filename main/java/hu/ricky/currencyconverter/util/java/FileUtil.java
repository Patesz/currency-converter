package hu.ricky.currencyconverter.util.java;

import hu.ricky.currencyconverter.User;
import hu.ricky.currencyconverter.model.Source;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public final class FileUtil{

    private FileUtil() {}

    public static String removeExtension (String fileName){
        String[] spl = fileName.split("\\.");
        return spl[0];
    }

    public static String removeFileStrFromURL(URL urlPath){
        String path = String.valueOf(urlPath);
        final String remove = "file:/";
        if(path.contains(remove)){
            return path.substring(remove.length());
        }
        return path;
    }

    public static List<String> getFileNamesInFolder(final File folder) {
        List<String> fileNames = new ArrayList<>();

        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                getFileNamesInFolder(fileEntry);
            } else {
                System.out.println(fileEntry);
                fileNames.add(fileEntry.getName());
            }
        }
        return fileNames;
    }

    public static List<String> readLines(String filePath) throws IOException {
        // Character coding can be an issue is some cases.
        return Files.readAllLines(Paths.get(filePath), StandardCharsets.ISO_8859_1);
    }

    public static String read(String filePath) throws IOException {
        // Character coding can be an issue is some cases.
        return Files.readString(Paths.get(filePath), StandardCharsets.ISO_8859_1);
    }

    // true if file is just created
    // false if it's already exists
    public static boolean createFileIfNotExists(Path filePath) throws IOException {
        Files.createDirectories(filePath.getParent());
        if(!Files.exists(filePath)) {
            Files.createFile(filePath);
            return true;
        }
        return false;
    }

    public static boolean isFileEmpty(Path fullPath) throws IOException {
        return Files.exists(fullPath) && Files.size(fullPath) <= 0 && !Files.isDirectory(fullPath);
    }

    public static void delete(Path filePath) throws IOException {
        Files.delete(filePath);
    }

    public static void create(String filePath, ArrayList<String> rows) throws IOException {
        Files.createFile(Paths.get(filePath));
        Files.write(Paths.get(filePath), rows);
    }

    // !!! DELETES FILES, BE CAUTIOUS !!!
    public static void recursiveDelete(Path path) throws IOException {
        try (Stream<Path> walk = Files.walk(path)) {
            walk.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(System.out::println)
                    .forEach(File::delete);
        }
    }

    public static List<Path> getPathsInDirectory(Path directory) throws IOException, NullPointerException {

        List<Path> pathList = new ArrayList<>();
        Files.createDirectories(directory);

        Files.walk(directory, FileVisitOption.FOLLOW_LINKS).forEach(pathList::add);

        if(pathList.size() <= 1){
            throw new NullPointerException("No backup found!");
        }

        return pathList;
    }

    public static Path getPathInDirectory (int backupIndex) throws IOException, NullPointerException, IndexOutOfBoundsException {
        List<Path> backups = FileUtil.getPathsInDirectory(Paths.get(User.getUserStorage() +"/"+ Source.getName()));
        return backups.get(backups.size() - backupIndex);
    }

}
