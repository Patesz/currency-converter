package hu.ricky.currencyconverter.util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import java.util.*;

public final class Util {

    private Util(){}

    public static ObservableList<Image> getImagesInDirectory(String imgDir, ObservableList<String> imageNameList, String imgFormat){
        ObservableList<Image> imageList = FXCollections.observableArrayList();
        for (String imageName : imageNameList) {
            Image img = new Image(imgDir + imageName + imgFormat);
            imageList.add(img);
        }
        return imageList;
    }

    public static List<Integer> getFilteredIndexList(final List<String> list, final String search){
        List<Integer> intList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            if(list.get(i).toLowerCase().contains(search.toLowerCase())){
                intList.add(i);
            }
        }
        return intList;
    }

    public static <T> List<T> getObjectListByIndexes(final List<T> list, final List<Integer> indexesToAdd){
        List<T> filteredList = new ArrayList<>();
        for (Integer elem : indexesToAdd){
            filteredList.add(list.get(elem));
        }
        return filteredList;
    }

}
