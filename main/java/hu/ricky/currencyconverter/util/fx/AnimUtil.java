package hu.ricky.currencyconverter.util.fx;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public final class AnimUtil {

    private AnimUtil(){}

    public static TranslateTransition translate(double duration, Node node, double fromX, double toX){
        TranslateTransition tt = new TranslateTransition(Duration.millis(duration), node);
        tt.setFromX(fromX);
        tt.setToX(toX);
        tt.play();
        return tt;
    }

    public static void fade(Node n, double duration, double fromVal, double toVal){
        FadeTransition ft = new FadeTransition(Duration.millis(duration), n);
        ft.setFromValue(fromVal);
        ft.setToValue(toVal);
        ft.play();
    }

}
