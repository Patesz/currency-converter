package hu.ricky.currencyconverter.util.fx;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.util.List;

public final class ComboBoxUtil {

    private ComboBoxUtil(){}

    public static void addImgList(final ComboBox<String> cb, final List<Image> imgList, final double fitHeight, final double fitWidth) {
        cb.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call (ListView <String> p) {
                return new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) { super.updateItem(item, empty);
                    setText(item);
                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        try {
                            ImageView iconImageView = new ImageView(imgList.get(this.getIndex()));
                            iconImageView.setFitHeight(fitHeight);
                            iconImageView.setFitWidth(fitWidth);
                            setGraphic(iconImageView);
                        } catch (IndexOutOfBoundsException e) {
                            System.out.println("Unhandled "+ e.getClass().getName());
                        }

                    }
                    }

                };
            }
        });
    }

    public static void scrollItems(final ComboBox<?> cb){
        cb.setOnScroll( e-> {
            if(e.getDeltaY() > 0){
                cb.getSelectionModel().selectPrevious();
            } else {
                cb.getSelectionModel().selectNext();
            }
        });
    }

}
