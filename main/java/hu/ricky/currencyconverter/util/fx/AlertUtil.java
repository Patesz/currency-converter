package hu.ricky.currencyconverter.util.fx;

import javafx.scene.control.Alert;

public final class AlertUtil {

    private AlertUtil(){}

    public static void error(String headerText, String contentText){
        AlertUtil.alert(headerText, contentText, Alert.AlertType.ERROR);
    }

    public static void warning(String headerText, String contentText){
        AlertUtil.alert(headerText, contentText, Alert.AlertType.WARNING);
    }

    private static void alert(String headerText, String contentText, Alert.AlertType at){
        Alert alert = new Alert(at);

        alert.setTitle(at.name()+"!");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();

    }


}
