package hu.ricky.currencyconverter.util.fx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public final class TextUtil {

	private TextUtil(){}

	public static void restrictTextFieldByRegex(final TextField tf, String regex) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(final ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches(regex)) {
					tf.setText(oldValue);
				}
			}
		});
	}

	public static void restrictTextFieldByRegex(final Label lbl, String regex) {
		lbl.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(final ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches(regex)) {
					lbl.setText(oldValue);
				}
			}
		});
	}

}
