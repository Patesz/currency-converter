package hu.ricky.currencyconverter.util.fx;

import javafx.scene.control.Spinner;

public final class SpinnerUtil{

    private SpinnerUtil(){}

	public static boolean handleSpinIfValueIsNull(final Spinner<Double> spin, final Double oldValue, final Double newValue) {
        if (newValue == null) {
            spin.getValueFactory().setValue(oldValue);
            return true;
        }
        return false;
    }

    public static boolean handleSpinIfValueIsNull(final Spinner<Integer> spin, final Integer oldValue, final Integer newValue) {
        if (newValue == null) {
            spin.getValueFactory().setValue(oldValue);
            return true;
        }
        return false;
    }
}
