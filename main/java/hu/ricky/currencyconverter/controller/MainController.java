package hu.ricky.currencyconverter.controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import hu.ricky.currencyconverter.User;
import hu.ricky.currencyconverter.model.SerializeDAO;
import hu.ricky.currencyconverter.model.Source;
import hu.ricky.currencyconverter.util.fx.AlertUtil;
import hu.ricky.currencyconverter.util.fx.AnimUtil;
import hu.ricky.currencyconverter.util.java.FileUtil;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.Duration;

import java.awt.*;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

final class PrevOptions implements Serializable {
    protected final Map<String, String> favPairs;

    protected PrevOptions(Map<String, String> favParam){
        this.favPairs = favParam;
    }
}

public final class MainController implements Initializable {

    @FXML private Button optionsBtn;
    @FXML private Button favBtn;

    @FXML private ContextMenu optionsMenu;
    @FXML private ContextMenu favMenu;

    @FXML public CurrController currencyController;
    @FXML public SettingsLayer settingsController;

    @FXML private BorderPane borderPane;
    @FXML private Label updateInfo;

    private SerializeDAO<PrevOptions> options;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            String favPath = User.getUserStorage()+"/favs.ser";
            options = new SerializeDAO<>(Paths.get(favPath));
            loadAllFav(options.readData().favPairs);
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Favourites are not loaded.");
            e.printStackTrace();
        }

        favBtn.setOnAction(e->favMenu.show(favBtn, Side.BOTTOM,-50,0));

        optionsBtn.setOnAction(e -> optionsMenu.show(optionsBtn, Side.BOTTOM,-50,0));

        Tooltip tp = new Tooltip("No favourites!");
        tp.setShowDelay(new Duration(100));
        tp.setHideDelay(new Duration(100));
        tp.setHideOnEscape(true);

        favBtn.setOnMouseEntered(e->{
            if(favMenu.getItems().size() > 0){
                favBtn.setTooltip(null);
            } else {
                favBtn.setTooltip(tp);
            }
        });

        borderPane.addEventFilter(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            if(settingsController.isOpen()){
                settingsController.hide();
                AnimUtil.fade(borderPane, 600.0, 0.7, 1.0);
            }
        });

        settingsController.getSwitchSourceBtn().setOnAction(event -> {
            try {
                Source.setURL(settingsController.getSourceText());

                currencyController.init(false);
                setDateLbl();
            } catch (MalformedURLException e) {
                AlertUtil.error("Invalid URL!", "'"+settingsController.getSourceText()+"' is not an URL!");
            }
        });
    }

    public void setDateLbl(){
        try {
            String[] dateSplit = currencyController.getUpdateDate().split("-");
            Month month = Month.of(Integer.parseInt(dateSplit[1]));
            updateInfo.setText("Updated on: " + month.getDisplayName(TextStyle.FULL, Locale.ENGLISH) + " " + dateSplit[2] + ", " + dateSplit[0]);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadAllFav(Map<String, String> favPairs){
        for (Map.Entry<String, String> fav : favPairs.entrySet()){
            MenuItem mi = createFav(favMenu, fav.getKey() + " -> "+ fav.getValue());
            favMenu.getItems().add(mi);
        }
    }

    private void setSelectedFav(String text){
        String noSpace = text.replace(" ", "");
        String[] splitText = noSpace.split("->");

        currencyController.resetDefaultFromState();
        currencyController.resetDefaultToState();

        boolean isFrom = false, isTo = false;

        for (String fullName : currencyController.getFullNameList()){
            if(splitText[0].equals(fullName.substring(0,3))){
                currencyController.setFromCurrencyValue(fullName);
                isFrom = true;
            }
            else if(splitText[1].equals(fullName.substring(0,3))){
                currencyController.setToCurrencyValue(fullName);
                isTo = true;
            }
        }

        if(!isFrom){
            AlertUtil.error(splitText[0]+" is not present.", splitText[0]+" has no value.");
        }
        if(!isTo){
            AlertUtil.error(splitText[1]+" is not present.", splitText[1]+" has no value.");
        }
    }

    @FXML
    private void addFav() {
        String from = currencyController.getFromCurrencyValue().substring(0,3);
        String to = currencyController.getToCurrencyValue().substring(0,3);

        for (MenuItem fav : favMenu.getItems()){
            if(fav.getText().equals(from + " -> " + to)){
                return;
            }
        }

        MenuItem cmi = createFav(favMenu, from + " -> " + to);

        favMenu.getItems().add(cmi);
    }

    private MenuItem createFav(ContextMenu contextMenu, String name){
        Button removeItemBtn = new Button(null, new FontAwesomeIconView(FontAwesomeIcon.TRASH_ALT, "16"));
        removeItemBtn.getStyleClass().add("menuButton");

        MenuItem mi = new MenuItem(name);
        mi.setGraphic(removeItemBtn);
        mi.setOnAction(e-> setSelectedFav(mi.getText()));

        removeItemBtn.setOnAction(e-> contextMenu.getItems().remove(mi));

        return mi;
    }

    @FXML
    private void restoreDef(){
        currencyController.resetDefaultFromState();
        currencyController.resetDefaultToState();
        settingsController.resetDefault();
        this.clearFavs();

        try {
            FileUtil.recursiveDelete(User.getUserStorage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void aboutInfo(){
        URL aboutFile = getClass().getResource("/about.html");

        try {
            Desktop.getDesktop().browse(aboutFile.toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openSettings() {
        settingsController.show();

        AnimUtil.fade(borderPane, 600.0, 1.0, 0.7);
    }

    public void storeFav() throws IOException {
        Map<String, String> favMap = new TreeMap<>();
        for (MenuItem fav : favMenu.getItems()) {
            String[] favSplit = fav.getText().replaceAll(" ", "").split("->");
            favMap.put(favSplit[0], favSplit[1]);
        }
        PrevOptions po = new PrevOptions(favMap);

        options.storeData(po);
    }

    private void clearFavs(){
        favMenu.getItems().clear();
    }

    public void storeAll() throws IOException {
        currencyController.storePrevData();
        settingsController.storeTheme();
        this.storeFav();
    }

}
