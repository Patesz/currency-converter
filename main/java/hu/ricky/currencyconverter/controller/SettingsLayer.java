package hu.ricky.currencyconverter.controller;

import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import de.jensd.fx.glyphs.weathericons.WeatherIconView;

import hu.ricky.currencyconverter.ApplicationLoader;
import hu.ricky.currencyconverter.User;
import hu.ricky.currencyconverter.model.SerializeDAO;
import hu.ricky.currencyconverter.model.Source;
import hu.ricky.currencyconverter.util.Util;
import hu.ricky.currencyconverter.util.fx.AnimUtil;
import hu.ricky.currencyconverter.util.fx.ComboBoxUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

final class ColorCollection {
    protected static final ObservableList<String> colors = FXCollections.observableArrayList
            ("BLUE", "BROWN", "GREEN", "ORANGE", "RED");
}

final class PrevTheme implements Serializable {
    private static final long serialVersionUID = 3L;

    public final String prevColor;
    public final boolean prevDark;
    public final int prevDigit;
    public final URL sourceURL;

    public PrevTheme(String col, boolean dark, int digit, String sourceURL) throws MalformedURLException {
        this.prevColor = col;
        this.prevDark = dark;
        this.prevDigit = digit;
        this.sourceURL = new URL(sourceURL);
    }

}

public final class SettingsLayer implements Initializable{

    @FXML private StackPane stackPane;
    @FXML private Slider digitSlider;
    @FXML private Label digitLabel;

    @FXML private ComboBox<String> colorPicker;
    @FXML private TextField source;
    @FXML private Button darkToggle;
    @FXML private Button switchSource;

    private static SerializeDAO<PrevTheme> serializedTheme = null;

    private static final Signature signature = new Signature();

    static class Signature {
        private Signature() {}
    }

// region Getters and Setters
    Button getSwitchSourceBtn(){ return switchSource; }
    String getSourceText(){ return source.getText(); }
// endregion

// region Initialize
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        digitSlider.valueProperty().addListener((observable, oldvalue, newvalue) ->
        {
            int val = digitSlider.valueProperty().intValue();
            digitLabel.setText(Integer.toString(val));
            Platform.runLater(() -> exchangeWithPrecision(val));
        });
    }

    public void init() {
        initColorPicker();

        serializedTheme = new SerializeDAO<>(Paths.get(User.getUserStorage()+"/prevTheme.ser"));

        try {
            PrevTheme pt = serializedTheme.readData();
            setTheme(pt.prevDark, pt.prevColor, pt.prevDigit, pt.sourceURL);
        } catch (IOException | ClassNotFoundException e){
            setTheme(false, "BLUE", 4, null);
        }

        darkToggle.setOnAction(e -> toggleDark());
    }

    private void initColorPicker(){
        colorPicker.getItems().addAll(ColorCollection.colors);

        ObservableList<Image> colorImgs = Util.getImagesInDirectory("/img/colors/", ColorCollection.colors, ".png");
        ComboBoxUtil.addImgList(colorPicker, colorImgs, 30,30);
    }

// endregion

// region Theme
    private void toggleDark(){
        if (getIsDark()) {
            switchBright();
        } else {
            switchDark();
        }
        CurrController.getInstance(signature).updateChgVal();
    }

    private void switchBright(){
        ApplicationLoader.getScene().getStylesheets().remove("/css/dark/DARK.css");
        ApplicationLoader.getScene().getStylesheets().add("/css/dark/WHITE.css");

        darkToggle.setText("On");
        WeatherIconView moon = new WeatherIconView(WeatherIcon.MOON_ALT_WAXING_GIBBOUS_2, "22");
        darkToggle.setGraphic(moon);
    }

    private void switchDark(){
        ApplicationLoader.getScene().getStylesheets().remove("/css/dark/WHITE.css");
        ApplicationLoader.getScene().getStylesheets().add("/css/dark/DARK.css");

        darkToggle.setText("Off");
        WeatherIconView sun = new WeatherIconView(WeatherIcon.YAHOO_32, "20");
        darkToggle.setGraphic(sun);
    }

    private void setTheme(boolean setDark, String color, int digit, URL prevURL) {
        if (setDark) {
            switchDark();
        } else {
            switchBright();
        }

        colorPicker.setValue(color);
        digitSlider.setValue(digit);

        for (String col : ColorCollection.colors){
            ApplicationLoader.getScene().getStylesheets().remove("/css/color/" + col + ".css");
        }

        ApplicationLoader.getScene().getStylesheets().add("/css/color/" + color + ".css");

        // If no previous source exists, leave as default source
        if (prevURL != null) {
            Source.setURL(prevURL);
            source.setText(prevURL.toString());
        } else {
            source.setText(Source.getURLAsString());
        }
    }

    public void storeTheme() throws IOException {
        try {
            serializedTheme.storeData(new PrevTheme(colorPicker.getValue(), getIsDark(), digitSlider.valueProperty().intValue(), source.getText()));
        } catch (MalformedURLException murle){
            serializedTheme.storeData(new PrevTheme(colorPicker.getValue(), getIsDark(), digitSlider.valueProperty().intValue(), Source.getURLAsString()));
        }
    }

    static StringBuilder decimalFormat = new StringBuilder("#,###");
    private void exchangeWithPrecision(int val){
        decimalFormat.delete(4, decimalFormat.length()-1);
        for (int i = 0; i < val; i++) {
            if(i==0) {
                decimalFormat.append(".");
            }
            decimalFormat.append("#");
        }

        CurrController cc = CurrController.getInstance(signature);
        cc.exchange();
    }

    public void changeColor(){
        String color = colorPicker.getValue();

        for (String col : colorPicker.getItems()) {
            ApplicationLoader.getScene().getStylesheets().remove("/css/color/" + col + ".css");
        }
        ApplicationLoader.getScene().getStylesheets().add("/css/color/"+color+".css");
    }

    private boolean getIsDark(){
        return !ApplicationLoader.getScene().getStylesheets().contains("/css/dark/WHITE.css");
    }

// endregion

// region MainController's methods

    boolean isOpen(){
        return stackPane.isVisible();
    }

    void show() {
        stackPane.setVisible(true);
        StackPane.setAlignment(stackPane, Pos.CENTER_LEFT);
        AnimUtil.translate(600, stackPane, -190, 0);
    }

    void hide() {
        AnimUtil.translate(600, stackPane, 0, -190).setOnFinished(e -> stackPane.setVisible(false));
    }

    // FXML injected method
    void resetDefault() {
        setTheme(false, "BLUE", 4, Source.getDefaultURL());
        CurrController.getInstance(signature).init(false);
    }

// endregion

}
