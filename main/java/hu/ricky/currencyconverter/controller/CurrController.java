package hu.ricky.currencyconverter.controller;

import hu.ricky.currencyconverter.ApplicationLoader;
import hu.ricky.currencyconverter.User;
import hu.ricky.currencyconverter.model.*;
import hu.ricky.currencyconverter.util.Util;
import hu.ricky.currencyconverter.util.fx.*;
import hu.ricky.currencyconverter.util.java.FileUtil;
import hu.ricky.currencyconverter.util.java.MathUtil;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

class PrevInput implements Serializable {
    private static final long serialVersionUID = 2L;

    public final double value;
    public final String from;
    public final String to;

    public PrevInput(double value, String from, String to){
        this.value = value;
        this.from = from;
        this.to = to;
    }
}

public final class CurrController implements Initializable {

    @FXML private Label fromToLbl;
    @FXML private Label resultLbl;
    @FXML private Label currencyTag;
    @FXML private Label fromSearch, toSearch;
    @FXML private Label infoLbl;

    @FXML private Label changeDateLbl;
    @FXML private Label chgLbl;

    @FXML private ComboBox<String> fromCb, toCb;

    @FXML private Spinner<Double> amountSpinner;

    private SerializeDAO<PrevInput> prevSer = null;

    private CurrencyDAO latestCurrDAO;
    private CurrencyDAO backupCurrDAO;

    private Map<String, Double> latestCurrMap = null;
    private Map<String, Double> backupCurrMap = null;

    private final Map<String, Image> stringImageMap = new TreeMap<>();

    private boolean isBackup = false;

// region Getters & Setters

    String getFromCurrencyValue(){ return fromCb.getValue(); }

    String getToCurrencyValue(){ return toCb.getValue(); }

    void setFromCurrencyValue(String val){
        fromCb.setValue(val);
    }

    void setToCurrencyValue(String val){
        toCb.setValue(val);
    }

    void resetDefaultFromState(){
        fromSearch.setText("");
        List<String> keys = new ArrayList<>(stringImageMap.keySet());
        resetCurrency(fromCb, fromSearch, keys, new ArrayList<>(stringImageMap.values()), keys.get(0));
    }
    void resetDefaultToState(){
        toSearch.setText("");
        List<String> keys = new ArrayList<>(stringImageMap.keySet());
        resetCurrency(toCb, toSearch, keys, new ArrayList<>(stringImageMap.values()), keys.get(1));
    }

// endregion

// region Instance

    private static CurrController instance;

    public CurrController() {
        instance = this;
    }

    static CurrController getInstance(SettingsLayer.Signature sg) {
        if (sg != null) {
            return instance;
        }
        throw new NullPointerException("Method signature is invalid!");
    }

// endregion

// region Initialize

    // region Initialize CurrencyData

    String getUpdateDate() {
        return latestCurrDAO.getData("date");
    }

    private boolean isCurrencyInitialized() {
        try {
            String data = Source.getAPIdata(Source.getURL());

            latestCurrDAO = new CurrencyDAO();
            latestCurrDAO.parseJSON(data);
            latestCurrDAO.setFilePath(Paths.get(User.getUserStorage()+"/"+Source.getName()+"/"+this.getUpdateDate()+".json"));

            latestCurrMap = latestCurrDAO.getRates();

            if(!latestCurrMap.containsKey(latestCurrDAO.getData("base"))) {
                latestCurrMap.put(latestCurrDAO.getData("base"), 1.0);
            }

            latestCurrDAO.write(latestCurrDAO.getFileContent());

            return true;
        } catch (Exception e) {
            try {
                e.printStackTrace();
                // Failed to reach API and/or failed to retrieve data, searching for backup...
                latestCurrDAO = new CurrencyDAO();
                latestCurrMap = getBackup(latestCurrDAO, 1);

                AlertUtil.warning("Failed to retrieve data from API!", "Check your internet connection or source URL.\nBackup data found with this source URL.");

                return true;
            } catch (Exception ex) {
                // No backup present, display error message to user.
                AlertUtil.error("Error: No data is available!", "Connect to the internet to get latest data");
                return false;
            }
        }

    }

    private Map<String, Double> getBackup(CurrencyDAO currDAO, int backupIndex) throws IOException, ParseException, IndexOutOfBoundsException {
        Path backupPath = FileUtil.getPathInDirectory(backupIndex);
        currDAO.setFilePath(backupPath);
        currDAO.parseJSON(currDAO.read());
        return currDAO.getRates();
    }

    // endregion

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        // Digits of length 0-7, digits of length 0-6 after dot, ()? means it occurs only once or not at all
        TextUtil.restrictTextFieldByRegex(amountSpinner.getEditor(), "\\d{0,15}([\\.]\\d{0,6})?");
        TextUtil.restrictTextFieldByRegex(fromSearch, "^[a-zA-Z -]{0,16}$");
        TextUtil.restrictTextFieldByRegex(toSearch, "^[a-zA-Z -]{0,16}$");

        amountSpinner.valueProperty().addListener((observableValue, oldValue, newValue)
                -> SpinnerUtil.handleSpinIfValueIsNull(amountSpinner, oldValue, newValue));

        amountSpinner.getEditor().textProperty().addListener((observableValue, oldValue, newValue)
                -> Platform.runLater(this::exchange));

        init(true);

    }

    List<String> getFullNameList(){
        return new ArrayList<>(this.stringImageMap.keySet());
    }

    public void init(boolean isInitPrevInput) {
        fromCb.getItems().clear();
        toCb.getItems().clear();

        if(isCurrencyInitialized()) {
            infoLbl.setText("Exchange rates are provided by: " + Source.getName());

            for (String currShortName : latestCurrMap.keySet()){
                String country = CurrencyCollection.getCountry(currShortName).replaceAll(" ", "-").toLowerCase()+".png";
                stringImageMap.put(currShortName+" - "+CurrencyCollection.getCurrency(currShortName), new Image("/img/countryflags30x30/"+ country));
            }

            fromCb.getItems().addAll(stringImageMap.keySet());
            toCb.getItems().addAll(stringImageMap.keySet());

            ComboBoxUtil.addImgList(fromCb, new ArrayList<>(stringImageMap.values()), 30,30);
            ComboBoxUtil.addImgList(toCb, new ArrayList<>(stringImageMap.values()), 30,30);

            ComboBoxUtil.scrollItems(fromCb);
            ComboBoxUtil.scrollItems(toCb);

            List<String> fromKeys = new ArrayList<>(stringImageMap.keySet());
            List<String> toKeys = new ArrayList<>(stringImageMap.keySet());
            if (isInitPrevInput) {
                try {
                    initPrevInput();
                } catch (IOException | ClassNotFoundException e) {
                    initInput(1.0, fromKeys.get(0) , fromKeys.get(1));
                }
            } else {
                initInput(1.0, fromKeys.get(0), fromKeys.get(1));
            }
            keyEventHandling(fromCb, fromSearch, fromKeys, new ArrayList<>(stringImageMap.values()));
            keyEventHandling(toCb, toSearch, toKeys, new ArrayList<>(stringImageMap.values()));

            currencyTag.setText(fromCb.getValue().substring(0, 3));

            try {
                backupCurrDAO = new CurrencyDAO();
                backupCurrMap = getBackup(backupCurrDAO, 2);
                isBackup = true;
                updateChgVal();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private void initInput(double value, String from, String to) {
        initInputFields(value, from, to);
    }

    private void initPrevInput() throws IOException, ClassNotFoundException {
        prevSer = new SerializeDAO<>(Paths.get(User.getUserStorage() + "/prevData.ser"));
        PrevInput pi = prevSer.readData();
        initInput(pi.value, pi.from, pi.to);
    }

    private void keyEventHandling(final ComboBox<String> cb, final Label lbl, final List<String> itemNames, final List<Image> originalImgs) {
        cb.setOnKeyPressed(event -> {
            KeyCode keyCode = event.getCode();
            if (keyCode.isLetterKey() || keyCode.getName().equals("Backspace") && lbl.getText().length() > 0) {
                if (keyCode.isLetterKey()) {
                    lbl.setText(lbl.getText() + keyCode.getName());
                    resetCurrency(cb, lbl, itemNames, originalImgs, cb.getItems().get(0));
                    cb.getSelectionModel().select(0);
                } else {
                    lbl.setText(lbl.getText().substring(0, lbl.getText().length() - 1));
                    resetCurrency(cb, lbl, itemNames, originalImgs, cb.getValue());
                }

                cb.show();

                exchange();
            }
        });
    }

    private void resetCurrency(final ComboBox<String> cb, final Label lbl, final List<String> itemNames, final List<Image> originalImgs, String value){
        List<Integer> hits = Util.getFilteredIndexList(itemNames, lbl.getText());
        if (hits.size() != 0) {
            cb.getItems().clear();
            cb.getItems().addAll(Util.getObjectListByIndexes(itemNames, hits));

            ComboBoxUtil.addImgList(cb, Util.getObjectListByIndexes(originalImgs, hits), 30,30);

            cb.setValue(value);

            lbl.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, new CornerRadii(3), Insets.EMPTY)));

            updateVisibleRowCount(cb, Math.min(cb.getItems().size(), 10));
        } else {
            lbl.setBackground(new Background(new BackgroundFill(Color.INDIANRED, new CornerRadii(3), Insets.EMPTY)));
        }
    }

    private void updateVisibleRowCount(ComboBox<?> cb, int rowCount){
        if(rowCount > 0 && (cb.getVisibleRowCount() != rowCount)) {
            cb.hide();
            cb.setVisibleRowCount(rowCount);
        }
    }

    private void initInputFields(double spinVal, String selectedFrom, String selectedTo){
        amountSpinner.getValueFactory().setValue(spinVal);
        fromCb.setValue(selectedFrom);
        toCb.setValue(selectedTo);
    }

// endregion

// region Result

    @FXML
    private void fromCbOnAction(){
        //Runs right after clearing listView so needs to be delayed
        Platform.runLater(() -> {
            currencyTag.setText(fromCb.getValue().substring(0, 3));
            exchange();
        });
    }

    @FXML
    private void toCbOnAction(){
        Platform.runLater(this::exchange);
    }

    private void setChgValLbl(Double chg) {
        String date = backupCurrDAO.getData("date");

        changeDateLbl.setText("Change from "+ date +" is:");

        boolean addPlus = false;
        if(1- chg > 0){
            addPlus = true;
            chgLbl.setStyle("-fx-text-fill: GREEN");
        } else if(1- chg < 0) {
            chgLbl.setStyle("-fx-text-fill: RED");
        } else {
            if(ApplicationLoader.getScene().getStylesheets().contains("/css/dark/DARK.css"))
                chgLbl.setStyle("-fx-text-fill: WHITE");
            else
                chgLbl.setStyle("-fx-text-fill: BLACK");
        }

        chgLbl.setText(MathUtil.roundNumber((1 - chg), SettingsLayer.decimalFormat + "%"));
        if(addPlus) chgLbl.setText("+" + chgLbl.getText());

    }

    private Double getChgVal(){

        Double fromChg = latestCurrMap.get(fromCb.getValue().substring(0, 3)) / backupCurrMap.get(fromCb.getValue().substring(0, 3));
        Double toChg = latestCurrMap.get(toCb.getValue().substring(0, 3)) / backupCurrMap.get(toCb.getValue().substring(0, 3));

        return fromChg / toChg;
    }

    void updateChgVal(){
        if(isBackup) setChgValLbl(getChgVal());
    }

    void exchange() {
        try {
            double amount = Double.parseDouble(amountSpinner.getEditor().getText());
            exchange(amount, fromCb.getValue().substring(0,3), toCb.getValue().substring(0,3));
            updateChgVal();
        } catch (NumberFormatException ignored) { }
    }

    void exchange(final double amount, final String from, final String to){
        try {
            double sub = latestCurrMap.get(to) / latestCurrMap.get(from);
            resultLbl.setText(MathUtil.roundNumber(sub * amount, SettingsLayer.decimalFormat.toString()) + " " + to);
            fromToLbl.setText(MathUtil.roundNumber(amount, SettingsLayer.decimalFormat.toString()) + " " + from + ":");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void reverseCurrency() {
        final String fromSearchText = fromSearch.getText();
        fromSearch.setText(toSearch.getText());
        toSearch.setText(fromSearchText);

        final String fromVal = fromCb.getValue();
        resetCurrency(fromCb, fromSearch, new ArrayList<>(stringImageMap.keySet()), new ArrayList<>(stringImageMap.values()), toCb.getValue());
        resetCurrency(toCb, toSearch, new ArrayList<>(stringImageMap.keySet()), new ArrayList<>(stringImageMap.values()), fromVal);

        exchange();
    }

// endregion

// region CloseApp

    void storePrevData() throws IOException {
        double validValue;
        if(amountSpinner.getEditor().getText().isEmpty()){
            validValue = amountSpinner.getValue();
        } else {
            validValue = Double.parseDouble(amountSpinner.getEditor().getText());
        }

        prevSer.storeData(new PrevInput(validValue, fromCb.getValue(), toCb.getValue()));
    }

// endregion

}
