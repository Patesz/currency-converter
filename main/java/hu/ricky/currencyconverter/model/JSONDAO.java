package hu.ricky.currencyconverter.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.nio.file.Path;

public class JSONDAO extends FileDAO {

    protected JSONObject jsonObject = new JSONObject();

    public JSONDAO() { }

    public JSONDAO(Path p) {
        super(p);
    }

    public void parseJSON(String content) throws ParseException {
        this.fileContent = content;
        jsonObject = (JSONObject) new JSONParser().parse(content);
    }

    public <X> X getData(String key) throws RuntimeException{
        return (X)jsonObject.get(key);
    }

}
