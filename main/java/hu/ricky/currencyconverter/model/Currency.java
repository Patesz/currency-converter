package hu.ricky.currencyconverter.model;

public class Currency {

    private String code;
    private String currency;

    public Currency(String code, String currency){
        this.code = code;
        this.currency = currency;
    }


    public String getCode() {
        return this.code;
    }

    public String getCurrency() {
        return this.currency;
    }
}
