package hu.ricky.currencyconverter.model;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public final class CurrencyCollection {

    private CurrencyCollection() {}

    private final static Map<String, Currency> allCurrency = new TreeMap<String, Currency>() {{
        put("United Arab Emirates", new Currency("AED", "UEA Dirham"));
        put("Argentina", new Currency("ARS", "Argentine Peso"));
        put("Australia", new Currency("AUD", "Australian Dollar"));
        put("Bulgaria", new Currency("BGN", "Bulgarian Lev"));
        put("Brazil", new Currency("BRL", "Brazilian Real"));
        put("Bahamas", new Currency("BSD", "Bahamian Dollar"));
        put("Canada", new Currency("CAD", "Canadian Dollar"));
        put("Switzerland", new Currency("CHF", "Swiss Franc"));
        put("Chile", new Currency("CLP", "Chilean Peso"));
        put("China", new Currency("CNY", "Chinese Yuan"));
        put("Colombia", new Currency("COP", "Colombian Peso"));
        put("Czech Republic", new Currency("CZK", "Czech Koruna"));
        put("Denmark", new Currency("DKK", "Danish Krone"));
        put("Dominican Republic", new Currency("DOP", "Dominican Peso"));
        put("Egypt", new Currency("EGP", "Egyption Pound"));
        put("European Union", new Currency("EUR", "Euro"));
        put("Fiji", new Currency("FJD", "Fijian Dollar"));
        put("United Kingdom", new Currency("GBP", "British Pound"));
        put("Guatemala", new Currency("GTQ", "Guatemalan Quetzal"));
        put("Hong Kong", new Currency("HKD", "Hong Kong Dollar"));
        put("Croatia", new Currency("HRK", "Croation Kuna"));
        put("Hungary", new Currency("HUF", "Hungarian Forint"));
        put("Indonesia", new Currency("IDR", "Indonesian Rupiah"));
        put("Israel", new Currency("ILS", "Israeli Shekel"));
        put("India", new Currency("INR", "Indian Rupee"));
        put("Iceland", new Currency("ISK", "Icelandic Krona"));
        put("Japan", new Currency("JPY", "Japanese Yen"));
        put("South Korea", new Currency("KRW", "South Korean Won"));
        put("Mexico", new Currency("MXN", "Mexican Peso"));
        put("Norway", new Currency("NOK", "Norwegian Krone"));
        put("New Zealand", new Currency("NZD", "New Zealand Dollar"));
        put("Panama", new Currency("PAB", "Panamanian Balboa"));
        put("Peru", new Currency("PEN", "Peruvian Sol"));
        put("Philippines", new Currency("PHP", "Philippine Peso"));
        put("Pakistan", new Currency("PKR", "Pakistani Rupee"));
        put("Republic of Poland", new Currency("PLN", "Polish złoty"));
        put("Paraguay", new Currency("PYG", "Paraguayan Guarani"));
        put("Romania", new Currency("RON", "Romanian Leu"));
        put("Russia", new Currency("RUB", "Russian Ruble"));
        put("Saudi Arabia", new Currency("SAR", "Saudi Arabian Riyal"));
        put("Sweden", new Currency("SEK", "Swedish Krona"));
        put("Singapore", new Currency("SGD", "Singapore Dollar"));
        put("Thailand", new Currency("THB", "Thai Baht"));
        put("Turkey", new Currency("TRY", "Turkish Lira"));
        put("Taiwan", new Currency("TWD", "Taiwan New Dollar"));
        put("United States of America", new Currency("USD", "US Dollar"));
        put("Uruguay", new Currency("UYU", "Uruguay Peso"));
        put("Vietnam", new Currency("VND", "Vietnamese Dong"));
        put("South Africa", new Currency("ZAR", "South African Rand"));
        put("Afghanistan", new Currency("AFN","Afghan afghani"));
        put("Albania", new Currency("ALL","Albanian lek"));
        put("Algeria", new Currency("DZD","Algerian dinar"));
        put("Angola", new Currency("AOA","Angolan kwanza"));
        put("Anguilla", new Currency("XCD","East Caribbean dollar")); // Anguilla, Antigua and Barbuda
        put("Armenia", new Currency("AMD","Armenian dram"));
        put("Aruba", new Currency("AWG","Aruban florin"));
        put("Ascension Island", new Currency("SHP","Saint Helena pound"));
        put("Azerbaijan", new Currency("AZN","Azerbaijan manat"));
        put("Bahrain", new Currency("BHD","Bahraini dinar"));
        put("Bangladesh", new Currency("BDT","Bangladeshi taka"));
        put("Barbados", new Currency("BBD","Barbadian dollar"));
        put("Belarus", new Currency("BYN","Belarusian ruble"));
        put("Belize", new Currency("BZD","Belize dollar"));
        put("Benin", new Currency("XOF","West African CFA franc"));
        put("Bermuda", new Currency("BMD","Bermudian dollar"));
        put("Bhutan", new Currency("BTN","Bhutanese ngultrum"));
        put("Bolivia", new Currency("BOB","Bolivian boliviano"));
        put("Bosnia and Herzegovina", new Currency("BAM","Bosnia and Herzegovina convertible mark"));
        put("Botswana", new Currency("BWP","Botswana pula"));
        put("Brunei", new Currency("BND","Brunei dollar"));
        put("Burundi", new Currency("BIF","Burundi franc"));
        put("Cape Verde", new Currency("CVE","Cape Verdean escudo"));
        put("Cambodia", new Currency("KHR","Cambodian riel"));
        put("Central African Republic", new Currency("XAF","Central African CFA franc")); // Chad
        put("Cayman Islands", new Currency("KYD","Cayman Islands dollar"));
        put("Comoros", new Currency("KMF","Comorian franc"));
        put("Republic of the Congo", new Currency("CDF","Congolese franc"));
        put("Cook Islands", new Currency("CKD","Cook Islands dollar"));
        put("Costa Rica", new Currency("CRC","Costa Rican colon"));
        put("Cuba", new Currency("CUP","Cuban peso"));
        put("Curacao", new Currency("ANG","Netherlands Antillean guilder"));
        put("Eritrea", new Currency("ERN","Eritrean nakfa"));
        put("Swaziland", new Currency("SZL","Swazi lilangeni"));
        put("Ethiopia", new Currency("ETB","Ethiopian birr"));
        put("Falkland Islands", new Currency("FKP","Falkland Islands pound"));
        put("Faroe Islands", new Currency("FOK","Faroese krona"));
        put("French Polynesia", new Currency("XPF","CFP franc"));
        put("Gambia", new Currency("GMD","Gambian dalasi"));
        put("Georgia", new Currency("GEL","Georgian lari"));
        put("Ghana", new Currency("GHS","Ghanaian cedi"));
        put("Gibraltar", new Currency("GIP","Gibraltar pound"));
        put("Guernsey", new Currency("GGP","Guernsey Pound"));
        put("Guinea", new Currency("GNF","Guinean franc"));
        put("Guyana", new Currency("GYD","Guyanese dollar"));
        put("Haiti", new Currency("HTG","Haitian gourde"));
        put("Honduras", new Currency("HNL","Honduran lempira"));
        put("Iran", new Currency("IRR","Iranian rial"));
        put("Iraq", new Currency("IQD","Iraqi dinar"));
        put("Isle of Man", new Currency("IMP","Manx pound"));
        put("Jamaica", new Currency("JMD","Jamaican dollar"));
        put("Jersey", new Currency("JEP","Jersey pound"));
        put("Jordan", new Currency("JOD","Jordanian dinar"));
        put("Kazakhstan", new Currency("KZT","Kazakhstani tenge"));
        put("Kenya", new Currency("KES","Kenyan shilling"));
        put("Kuwait", new Currency("KWD","Kuwaiti dinar"));
        put("Kyrgyzstan", new Currency("KGS","Kyrgyzstani som"));
        put("Laos", new Currency("LAK","Lao kip"));
        put("Lebanon", new Currency("LBP","Lebanese pound"));
        put("Lesotho", new Currency("LSL","Lesotho loti"));
        put("Liberia", new Currency("LRD","Liberian dollar"));
        put("Libya", new Currency("LYD","Libyan dinar"));
        put("Macao", new Currency("MOP","Macanese pataca"));
        put("Madagascar", new Currency("MGA","Malagasy ariary"));
        put("Malawi", new Currency("MWK","Malawian kwacha"));
        put("Malaysia", new Currency("MYR","Malaysian ringgit"));
        put("Maldives", new Currency("MVR","Maldivian rufiyaa"));
        put("Mauritania", new Currency("MRU","Mauritanian ouguiya"));
        put("Mauritius", new Currency("MUR","Mauritian rupee"));
        put("Moldova", new Currency("MDL","Moldovan leu"));
        put("Mongolia", new Currency("MNT","Mongolian tugrik"));
        put("Morocco", new Currency("MAD","Moroccan dirham"));
        put("Mozambique", new Currency("MZN","Mozambican metical"));
        put("Myanmar", new Currency("MMK","Myanmar kyat"));
        put("Namibia", new Currency("NAD","Namibian dollar"));
        put("Nepal", new Currency("NPR","Nepalese rupee"));
        put("Nicaragua", new Currency("NIO","Nicaraguan cordoba"));
        put("Nigeria", new Currency("NGN","Nigerian naira"));
        put("North Korea", new Currency("KPW","North Korean won"));
        put("Macedonia", new Currency("MKD","Macedonian denar"));
        put("Oman", new Currency("OMR","Omani rial"));
        put("Papua New Guinea", new Currency("PGK","Papua New Guinean kina"));
        put("Qatar", new Currency("QAR","Qatari riyal"));
        put("Rwanda", new Currency("RWF","Rwandan franc"));
        put("Samoa", new Currency("WST","Samoan tala"));
        put("Sao Tome and Principe", new Currency("STN","Sao Tome and Principe dobra"));
        put("Serbia", new Currency("RSD","Serbian dinar"));
        put("Seychelles", new Currency("SCR","Seychellois rupee"));
        put("Sierra Leone", new Currency("SLL","Sierra Leonean leone"));
        put("Solomon Islands", new Currency("SBD","Solomon Islands dollar"));
        put("Somalia", new Currency("SOS","Somali shilling"));
        put("South Sudan", new Currency("SSP","South Sudanese pound"));
        put("Sri Lanka", new Currency("LKR","Sri Lankan rupee"));
        put("Sudan", new Currency("SDG","Sudanese pound"));
        put("Suriname", new Currency("SRD","Surinamese dollar"));
        put("Syria", new Currency("SYP","Syrian pound"));
        put("Tajikistan", new Currency("TJS","Tajikistani somoni"));
        put("Tanzania", new Currency("TZS","Tanzanian shilling"));
        put("Tonga", new Currency("TOP","Tongan pa’anga"));
        put("Trinidad and Tobago", new Currency("TTD","Trinidad and Tobago dollar"));
        put("Tunisia", new Currency("TND","Tunisian dinar"));
        put("Turkmenistan", new Currency("TMT","Turkmen manat"));
        put("Uganda", new Currency("UGX","Ugandan shilling"));
        put("Ukraine", new Currency("UAH","Ukrainian hryvnia"));
        put("Uzbekistan", new Currency("UZS","Uzbekistani som"));
        put("Vanuatu", new Currency("VUV","Vanuatu vatu"));
        put("Venezuela", new Currency("VES","Venezuelan bolivar"));
        put("Yemen", new Currency("YER","Yemeni rial"));
        put("Zambia", new Currency("ZMW","Zambian kwacha"));
    }};

    public static Map<String, Currency> getAllCurrency(){
        return CurrencyCollection.allCurrency;
    }

    public static Collection<Currency> getAllValue(){
        return CurrencyCollection.allCurrency.values();
    }

    public static String getCurrency(String code) throws NullPointerException{
        for (Currency c : CurrencyCollection.allCurrency.values()){
            if(c.getCode().equals(code)){
                return c.getCurrency();
            }
        }
        throw new NullPointerException("Currency with code: "+code+" is not present!");
    }

    public static String getCountry(String code){
        for (Map.Entry<String, Currency> entry : CurrencyCollection.allCurrency.entrySet()){
            if(entry.getValue().getCode().equals(code)){
                return entry.getKey();
            }
        }
        throw new NullPointerException("Country name with code: "+code+" is not present!");
    }


}
