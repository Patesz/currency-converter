package hu.ricky.currencyconverter.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

// https://api.exchangeratesapi.io/latest
// https://api.exchangerate-api.com/v4/latest/USD

public final class Source {

    private Source() {}

    private final static String defaultURL = "https://api.exchangerate-api.com/v4/latest/USD";

    public static URL getDefaultURL(){
        try {
            return new URL(defaultURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static URL url;
    static {
        try {
            url = new URL("https://api.exchangerate-api.com/v4/latest/USD");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static String name = "api.exchangerate-api.com";

    public static void setURL(URL url) {
        Source.url = url;
        Source.name = Source.url.getAuthority();
    }
    public static void setURL(String url) throws MalformedURLException {
        Source.url = new URL(url);
        Source.name = Source.url.getAuthority();
    }

    public static URL getURL(){
        return Source.url;
    }

    public static String getURLAsString(){ return Source.url.toString(); }

    public static String getName(){
        return name;
    }

    public static String getAPIdata(URL sourceURL) throws IOException {

        // Throws UnknownHostException
        InputStream stream = sourceURL.openStream();

        // Throws IOException
        return new BufferedReader(new InputStreamReader(stream)).readLine();

    }


}
