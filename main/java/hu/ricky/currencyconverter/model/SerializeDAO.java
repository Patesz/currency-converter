package hu.ricky.currencyconverter.model;

import hu.ricky.currencyconverter.util.java.FileUtil;

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class SerializeDAO<T> extends Serialize<T>{

    public SerializeDAO(Path filePath) {
        super(filePath);
    }

    public void storeData(T data) throws IOException {
        Files.createDirectories(filePath.getParent());
        if(!Files.exists(filePath)) {
            Files.createFile(filePath);
        }
        super.write(data);
    }

    public T readData() throws IOException, ClassNotFoundException {
        Files.createDirectories(filePath.getParent());
        if(!Files.exists(filePath)) {
            Files.createFile(filePath);
            throw new NoSuchFileException("File is just created, nothing to read from it!");
        } else {
            if(FileUtil.isFileEmpty(filePath)){
                throw new EOFException("File '" +filePath+"' is empty!");
            }
            return super.read();
        }
    }

}
