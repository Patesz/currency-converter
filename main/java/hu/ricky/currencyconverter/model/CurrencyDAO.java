package hu.ricky.currencyconverter.model;

import hu.ricky.currencyconverter.exc.UnsupportedCurrencyException;

import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

public final class CurrencyDAO extends JSONDAO {

    public CurrencyDAO(){}

    public CurrencyDAO(Path p){ super(p); }

    public Map<String, Double> getRates(){
        String allRates = jsonObject.get("rates").toString();
        allRates = allRates.replaceAll("\\{", "").replaceAll("}", "").replaceAll("\"",""); // .substring(1, allRates.length()-1);

        String[] ratesPair = allRates.split(",");

        Map<String, Double> rateMap = new TreeMap<>();
        for (String rate : ratesPair){
            String[] keyVal = rate.split(":");
            rateMap.put(keyVal[0], Double.parseDouble(keyVal[1]));
        }

        return rateMap;
    }

    public Double getRateVal(String currency) throws UnsupportedCurrencyException {
        Map<String, Double> rates = this.getRates();

        for (Map.Entry<String, Double> cp : rates.entrySet()){
            if(cp.getKey().equals(currency)){
                return cp.getValue();
            }
        }

        throw new UnsupportedCurrencyException(currency+" is not supported!");
    }

}
