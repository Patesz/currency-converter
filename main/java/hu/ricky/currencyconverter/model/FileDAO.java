package hu.ricky.currencyconverter.model;

import hu.ricky.currencyconverter.util.java.FileUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class FileDAO {

    public FileDAO(){ }

    public FileDAO(Path filePath){
        this.filePath = filePath;
    }

    protected Path filePath;
    protected String fileContent;

    public Path getFilePath(){
        return filePath;
    }
    public void setFilePath(Path p){ filePath = p; }

    public String getFileContent(){
        return fileContent;
    }

    public String read() throws IOException {
        if(!FileUtil.createFileIfNotExists(filePath)) {
            this.fileContent = Files.readString(filePath);
            return this.fileContent;
        }
        throw new NoSuchFileException("File is just created, nothing to read from it!");
    }

    public void write(String data) throws IOException {
        FileUtil.createFileIfNotExists(filePath);

        Files.write(filePath, data.getBytes());
    }

}
