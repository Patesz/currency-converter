package hu.ricky.currencyconverter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class User {

    private User() { }

    private static Path privateStorage;

    public static Path getUserStorage(){
        return privateStorage;
    }

    static void setPrivateStorage() throws IOException {
        String userHome = System.getProperty("user.home");
        privateStorage = Files.createDirectories(Paths.get(userHome + "/.ricky/currencyconverter/"));
    }

}
